﻿
#include <iostream>
#include "Animal.h"
using namespace std;

int main()
{
	Animal* animals[] = { new Cat(), new Dog(), new Pig() };
	for (int i = 0; i < 3; i++)
		animals[i]->Voice();
}