#include <iostream>
using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "Unidentified voice\n";
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Woof!\n";
	}
};
class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow!\n";
	}
};

class Pig : public Animal
{
public:
	void Voice() override
	{
		cout << "Oink!\n";
	}
};
